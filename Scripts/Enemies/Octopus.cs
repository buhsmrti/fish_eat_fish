﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Octopus : MonoBehaviour
{
    public float movespeed = 0.7f;

    public Vector2 userDirection;

    private float calories;

    // Start is called before the first frame update
    void Start()
    {
        float randnum = Random.Range(0.3f,2.5f);
        
        calories = randnum;

        transform.localScale = new Vector2(randnum,randnum);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(userDirection * movespeed * Time.deltaTime);
    }

    void OnBecameInvisible() {
        Destroy (gameObject);
    }

    public float getCalories()
    {
        return this.calories;
    }
}
