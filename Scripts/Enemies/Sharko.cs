﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sharko : MonoBehaviour
{
    public float movespeed = 0.05f;

    public Vector2 userDirection;

    public SpriteRenderer sprite;

    private float calories;

    public float acceleration = 2f;


    // Start is called before the first frame update
    void Start()
    {
        float randnum = Random.Range(1f,1.3f);
        
        calories = randnum;

        transform.localScale = new Vector2(randnum,randnum);

        sprite = GetComponent<SpriteRenderer>();
         if (userDirection == Vector2.left)
        {
            sprite.flipX = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(userDirection * movespeed * Time.deltaTime);
        if (movespeed < 0.4f) 
        {
            movespeed += acceleration/10 * Time.deltaTime;
        }
        else 
        {
            movespeed += acceleration * Time.deltaTime;
        }


    }

    void OnBecameInvisible() {
        Destroy (gameObject);
    }

    public float getCalories()
    {
        return this.calories;
    }
}
