﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFish : MonoBehaviour
{
    public float movespeed; // rychlost ryby

    public Vector2 userDirection; // směr ryby

    public SpriteRenderer sprite; // komponenta SpriteRenderer

    private float calories; // pomocná proměnná "kalorie"

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>(); // komponenta starající se o grafickou část objektu

        movespeed = Random.Range(0.5f, 1.0f); // přiřadíme náhodnou hodnotu pro rychlost pro větší variaci

        float randnum = Random.Range(0.4f, 3.5f); // vybereme náhodné číslo, které udává velikost ryby
        calories = randnum; // kalorie se rovnají velikosti ryby

        transform.localScale = new Vector2(randnum,randnum); // změna velikosti podle náhodného čísla

        if (userDirection == Vector2.left) // pokud je směr ryby doleva, překlop sprite podél osy X
        {
            sprite.flipX = true;
        }
    }

    void FixedUpdate()
    {
        transform.Translate(userDirection * movespeed * Time.deltaTime);
        /*
        Metoda Translate komponenty transform mění pozici dle vektoru, výsledkem je souvislý pohyb jedním směrem.
        userDirection = směrový vektor, Vector2.left odpovídá vektoru (-1,0), Vector2.right odpovídá vektoru (1,0)
        Time.deltaTime = Interval v sekundách od posledního snímku k aktuálnímu. To zajistí, že pohyb bude plynulý
        nezávisle na herní framerate.         
         */

    }

    void OnBecameInvisible() { // metoda, která se stará o zničení ryby, pokud vypluje z obrazovky
        Destroy (gameObject); // object "zničí" sám sebe
    }

    public float getCalories()
    {
        return this.calories;
    }
}
