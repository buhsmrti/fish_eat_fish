﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{

    public static int scoreValue = 0; // skóre začíná vždy na nule
    Text score; // textová proměnná

    void Awake () 
    { 
        /*
            Metodu Awake() dědíme z Monobehaviour. Automaticky se volá při načtení nové scény.

            Zde je nutné ji použít jelikož si přejeme, aby námi získané skóre
            přetrvalo mezi úrovněmi. Unity se totiž při každém přechodu do jíné scény
            zbaví všech objektů z předchozí, pokud tomu tedy sami nezabráníme 
            následující metodou DontDestroyOnLoad().
        */
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        score = GetComponent<Text>();  // přiřadíme textový komponent GUI objektu
    }

    void Update()
    {
        score.text = "Score: " + scoreValue; // updatujeme hodnotu získaného skóre
    }
}
