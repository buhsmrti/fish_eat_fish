using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour
{

    Text highScore; // textová proměnná

    void Start()
    {
        highScore = GetComponent<Text>();

        if (PlayerPrefs.HasKey("HighScore"))
        {
            highScore.text = "High Score: " + PlayerPrefs.GetInt("HighScore");
        }
        else
        {
            PlayerPrefs.SetInt("HighScore", 0);
            highScore.text = "High Score: " + PlayerPrefs.GetInt("HighScore");
        }
    }

    void Update()
    {
        
    }
}
