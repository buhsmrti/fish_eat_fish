using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed;             // rychlost
    private AudioSource crunch;     // audio komponenta
    private Rigidbody2D rigidBody;  // RB2D komponenta
    public SpriteRenderer sprite;   // sprite komponenta
    private Renderer[] renderers;   // seznam vykreslených stavů hráče
    private bool isWrappingX = false; // kontroluje, zda hráč přechází z jedné strany obrazovky na druhou
    private float calories;         // kalorická hodnota naší ryby

    void Start()
    {
        // přiřazení komponent do proměnných
        renderers = GetComponentsInChildren<Renderer>();
        rigidBody = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        crunch = GetComponent<AudioSource>();

        calories = 1f; // vždy začínáme s kalorickou hodnotou 1

        // efekt potopení při startu úrovně; jako by hráč skočil do vody
        rigidBody.AddForce(new Vector2(0f, -82f));

    }

    // Update is called once per frame
    void Update()
    {
        
        
        if(Input.GetKeyDown(KeyCode.P))
        {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        //Store the current horizontal input in the float moveHorizontal.
        float moveHorizontal = Input.GetAxis("Horizontal");

        //Flip the sprite according to the direction
        if (moveHorizontal < 0)
        {
            sprite.flipX = true;
        }
        else if (moveHorizontal > 0)
        {
            sprite.flipX = false;
        }

        //Store the current vertical input in the float moveVertical.
        float moveVertical = Input.GetAxis("Vertical");


        //Use the two store floats to create a new Vector2 variable movement.
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
        rigidBody.AddForce(movement * speed);

        //Speed limit
        rigidBody.velocity = Vector2.ClampMagnitude(rigidBody.velocity, 10f);
        

        
        ScreenWrap();   // kontrola screen-wrappingu

    }

    void ScreenWrap()
    {
        bool isVisible = CheckRenderers(); // je hráč viditelný = na obrazovce?

        if(isVisible)
        {
            isWrappingX = false; // je viditelný, není nutné nic provádět
            return;
        }

        if(isWrappingX) // probíhá screen-wrap, musíme tedy funkci utnout, jinak ryba zůstane "zaseklá" mimo obrazovku
        {
            return;
        }
        
        // sem se dostaneme, pokud vyplaveme z obrazovky
        Vector2 newPosition = transform.position; // aktuální souřadnice pozice hráče*

        if(newPosition.x > 1 || newPosition.x < 0)  // pokud jsme mimo kameru, tak:
        {
            newPosition.x = - newPosition.x;        // překlopíme naši X souřadnici
            isWrappingX = true;                     // probíhá screen-wrapping
        }

        transform.position = newPosition;           // přesuneme hráče na novou pozici

    }

    bool CheckRenderers()
    {
        // funkce projde seznam aktuálních vykreslených stavů hráče
        foreach (Renderer renderer in renderers)
        {
            if(renderer.isVisible)  // pokud alespoň 1 je viditelný, není důvod pro screen-wrap, vracíme true
            {
                return true;
            }
        }

        return false;  // jsme mimo obrazovku, vracíme false
    }

    void OnTriggerEnter2D (Collider2D other)  // jako argument bere komponentu objektu, na který jsme narazili
    {
        GameObject colObject = other.gameObject;  // "vytáhneme" si z komponenty, o jaký konktrétní objekt se jedná
        crunch.Play();  // zahrajeme zvuk křupnutí

        if (other.CompareTag("Fish"))  // ptáme se, se kterým typem nepřítele jsme se srazili
         {
            float enemyNutrition = colObject.GetComponent<EnemyFish>().getCalories();  //  kalorická hodnota nepřítele

            /*  Srovnáme kal. hodnoty hráče a nepřítele. 
                Kalorie nám udávají, který živočich je větší, tedy jestli je námi poživatelný
            */

            if (getCalories() >= enemyNutrition)  // ano, jsme větší/stejně velcí
            {
                ScoreScript.scoreValue += (int)(enemyNutrition*10);  // inkrementujeme herní skóre
                Grow(enemyNutrition);  // porosteme o hodnotu poměrně k velikosti snězeného živočicha
                Destroy(colObject);  // definitivně zničíme nepřítele
            }
            else // bohužel jsme byli pozřeni
            {
                SaveScore();  // uložení skóre
                SceneManager.LoadScene(0);  // okamžitý návrat do hlavního menu
            }
         }

        else if (other.CompareTag("Octo"))
         {
            float enemyNutrition = colObject.GetComponent<Octopus>().getCalories();
            if (getCalories() >= enemyNutrition*2)
            {
                ScoreScript.scoreValue += (int)(enemyNutrition*2*20);
                Grow(enemyNutrition*2f);
                Destroy(colObject);
            }
            else 
            {
                SaveScore();
                SceneManager.LoadScene(0);
            }
         }
        
        else if (other.CompareTag("Sharko"))
         {
            float enemyNutrition = colObject.GetComponent<Sharko>().getCalories();
            if (getCalories() >= enemyNutrition*3.44)
            {
                ScoreScript.scoreValue += (int)(enemyNutrition*3.44*30);
                Grow(enemyNutrition*3.44f);
                Destroy(colObject);
            }
            else 
            {
                SaveScore();
                SceneManager.LoadScene(0);
            }
         }
    }

    void Grow(float cal) // zvětšení hráče, jako argument bere kalorickou hodnotu snězeného nepřítele
    {
        /*  
            Hráči přiřadíme nový vektor velikosti, ten se rovná aktuálnímu 
            vektoru velikosti plus k X a Y souřadnicím přidáme část kalorií
            nepřítele (vydělena 10, jinak by růst byl příliš velký).

            Dále nastavíme našemu hráči novou kalorickou hodnotu, ta se rovná 
            aktuální velikosti X nebo Y souřadnice vektoru škály; je jedno,
            kterou použijeme.

            Nakonec zkotrolujeme podmínky výhry (=naše ryba je po zvětšení už příliš velká)
        */

        transform.localScale = new Vector2(transform.localScale.x + cal/10, transform.localScale.y + cal/10);
        this.calories = transform.localScale.x;

        checkWin();
    }

    public float getCalories()
    {
        return this.calories;
    }

    public void SaveScore()
    {
        // pokud je aktuání hodnota skóre vyšší, než zatím dosaženého high score, přemažeme ji
        if (ScoreScript.scoreValue > PlayerPrefs.GetInt("HighScore")) 
            {
                PlayerPrefs.SetInt("HighScore", ScoreScript.scoreValue);
            }
    }

    public void checkWin() // kontrola výherní podmínky
    {
        if (this.calories >= 16) // hodnota velikosti 16 je hard-coded, jedná se o velikost, kdy už hráč je příliš velký na
        {
            /* 
                Načteme scénu s hodnotou indexu o 1 větší, než je index aktuální scény => přechod do dalšího levelu.               
                Pomocí importu UnityEngine.SceneManagement můžeme řídit přechody mezi scénami přímo ve scriptu hráče.

                Scenemanager.LoadScene(index scény) = načte scénu s daným indexem.
                SceneManager.GetActiveScene().buildIndex = vrací celé číslo - index aktuální scény
            */

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);  
        }
    }

}
