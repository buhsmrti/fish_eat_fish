using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonHandler : MonoBehaviour
{
   public void StartGame()  // tlačítko "Play"
   {
       ScoreScript.scoreValue = 0;
       SceneManager.LoadScene(1);
   }

   public void HowToPlay() // tlačítko "How to play
   {
       SceneManager.LoadScene(5);
   }

   public void BackToMenu() // návrat do Main Menu
   {
       SceneManager.LoadScene(0);
   }

    public void BackToMenuScore() // návrat do Main Menu s uložením high score
   {
        if (ScoreScript.scoreValue > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", ScoreScript.scoreValue);
        }
       SceneManager.LoadScene(0);
   }

}
