﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishLSpawner : MonoBehaviour
{
    public GameObject enemy; // který nepřítel bude vytvořen, patří sem objekt typu prefab
    float randY; // náhodná Y pozice (u chobotnice X pozice)
    Vector2 whereToSpawn; // konečná pozice, kde se objekt objeví
    public float spawnrate = 2f; // časová prodleva v sekundách, kdy se objeví nový nepřítel
    float nextSpawn = 0.0f; // doba, za kterou se objeví další nepřítel
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnrate;
            randY = UnityEngine.Random.Range(-4.3f, 2.8f);
            whereToSpawn = new Vector2(transform.position.x, randY);
            Instantiate(enemy, whereToSpawn, Quaternion.identity);
        }
    }
}
